#!/bin/bash

# React
npm install -g yarn
#npm install --save live-server babel-cli 
npm install --save webpack webpack-dev-server 
npm i -S webpack@latest webpack-cli@latest 
npm install --save babel-core babel-loader
npm install --save react react-dom validator

# yarn init
# yarn add babel-preset-react@6.24.1 babel-preset-env@1.5.2
# babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch

# yarn run build
# "build-babel": "babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch"
