import moment from "moment/moment";

export default [{
    id: '1',
    description: 'coffee',
    note: '',
    amount: 200,
    createdAt: 0
}, {
    id: '2',
    description: 'rent',
    note: '',
    amount: 99000,
    createdAt: moment(0).subtract(4, 'days').valueOf()
}, {
    id: '3',
    description: 'gas',
    note: '',
    amount: 4000,
    createdAt: moment(0).add(4, 'days').valueOf()
}];
