const detail = {
    isVisible: false,
    message: "This is the detail that you can see"
};

const toggleVisibility = () => {
    detail.isVisible = !detail.isVisible;
    render();
}
const appRoot = document.getElementById("app");

const render = () => {
    const template = (
        <div>
            <h1>Visibility Toggle</h1>

            <button onClick={toggleVisibility}>{detail.isVisible ? "Hide details" : "Show details"}</button>
            {detail.isVisible && <p>{detail.message}</p>}
        </div>
    );
    ReactDOM.render(template, appRoot);
};

render();
