class VisibilityToggle extends React.Component {
    constructor(props) {
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            isVisible: false,
            message: "This is the detail that you can see"
        };
    }

    handleToggleVisibility() {
        this.setState((prevState) => {
            return {
                isVisible: !prevState.isVisible
            };
        });
    }

    render() {
        return (
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}>{this.state.isVisible ? "Hide details" : "Show details"}</button>
                {this.state.isVisible && <p>{this.state.message}</p>}
            </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById("app"));

